/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.databaseporject.model.dao;

import com.kittipon.databaseporject.model.User;
import java.util.List;

/**
 *
 * @author kitti
 */
public interface Dao<T> {

    T get(int id);

    List<T> getAll();

    T save(T obj);

    T update(T obj);

    int delete(T obj);

    List<T> getAllOrderBy(String name, String order);
}
