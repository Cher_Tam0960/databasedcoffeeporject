/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.databaseporject.model.dao.service;

import com.kittipon.databaseporject.model.User;
import com.kittipon.databaseporject.model.UserDao;

/**
 *
 * @author kitti
 */
public class UserService {

    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if (user.getPassword().equals(password)) {
            return user;
        }

        return null;
    }
}
